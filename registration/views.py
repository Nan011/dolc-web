from django.shortcuts import render
from django.http import JsonResponse
from registration import models

# Create your views here.
def regis(request):
    data = {}
    return render(request, "main-pages/regis.html", data)

def ielts(request):
    data = {}
    return render(request, "main-pages/ielts-form.html", data)

def event(request, topic):
    data = {}
    return render(request, "main-pages/" + topic + "-form.html", data)

def get_data(request, type):
    data = {
        "success": False,
    }
    if request.method == "GET" and (type.lower() in ["talkshow", "workshop", "etc"]):
        events = None
        if type == "talkshow":
            events = models.Talkshow.objects.all()
        elif type == "workshop":
            events = models.Workshop.objects.all()
        else:
            events = models.Event.objects.all()
            
        if events.first() != None:
            data["type"] = type
            data[type] = []
            for event in events:
                data[type].append({
                    "name": event.name, 
                    "place": event.place, 
                    "date": event.date,
                    "regisTime": event.regis_time,
                    "etc": additional(event, type)})
            data["success"] = True

    return JsonResponse(data)

def additional(event, type):
    if type == "talkshow":
        return [event.speaker]
    elif type == "workshop":
        return [
            event.agency.url,
            event.agency_name,
            event.speaker,
            event.requirements,
            event.registration_information
        ]