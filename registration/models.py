from django.db import models

# Create your models here.

class Event(models.Model):
    name = models.TextField(max_length=300)
    place = models.TextField(max_length=300)
    date = models.TextField(max_length=300)
    regis_time = models.CharField(max_length=50, null=True)
    def __str__(self):
        return self.name

class Talkshow(Event):
    speaker = models.TextField(max_length=300)

class Workshop(Event):
    agency = models.ImageField(upload_to="imgs/registration/agencies/", null=True, blank=True)
    agency_name = models.CharField(max_length=200, null=True, blank=True)
    speaker = models.TextField(max_length=300)
    requirements = models.TextField(max_length=300)
    registration_information = models.TextField(max_length=600)
