from django.contrib import admin
from registration import models
# Register your models here.
admin.site.register(models.Event)
admin.site.register(models.Talkshow)
admin.site.register(models.Workshop)