from django.urls import path
from registration import views

app_name = "regis";
urlpatterns = [
    path("", views.regis, name="regis"),
    path("etc/ielts/", views.ielts, name="ielts"),
    path("talkshow/<str:topic>/", views.event),
    path("workshop/<str:topic>/", views.event),
    path("<str:type>-data/", views.get_data),
]