from django.urls import path
from events import views

app_name = "events"
urlpatterns = [
    path("", views.index, name="events"),
    path("events-data/", views.get_event),
]