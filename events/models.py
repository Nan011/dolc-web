from django.db import models

# Create your models here.
class Event(models.Model):
    photo = models.ImageField(upload_to="imgs/events/", blank=True, null=True)
    title = models.CharField(max_length=100)
    description = models.TextField(max_length=2000)
    place = models.CharField(max_length=100, null=True)
    date = models.TextField(max_length=100, null=True)
    def __str__(self):
        return self.title