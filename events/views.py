from django.shortcuts import render
from django.http import JsonResponse
from events import models

# Create your views here.
def index(request):
    data = {
        "events": models.Event.objects.all(),
    }
    return render(request, "main-pages/events.html", data)

def get_event(request):
    data = {
        "success": False,
    }
    if request.method == "GET":
        events = models.Event.objects.all();
        if events.first() != None:
            data["events"] = []
            for event in events:
                data["events"].append({
                    "photo": event.photo.url,
                    "title": event.title,
                    "desc": event.description,
                    "place": event.place,
                    "date": event.date 
                })
            data["success"] = True
    return JsonResponse(data)