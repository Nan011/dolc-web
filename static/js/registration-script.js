var titleState = true;
var regisState = [];
var fetchState = {
    talkshow: false, 
    workshop: false, 
    etc: false
}

var regisTime = {
    talkshow: [],
    workshop: [],
    etc: [],
}

function animatetingRegis() {
    if (fetchState["talkshow"] && fetchState["workshop"] && fetchState["etc"]) {
        titlePos = $(".l-regis-section .c-regis-section__title .c-regis-section__line-art").offset().top - $(window).scrollTop();
        if (titleState && titlePos >= -250 && titlePos <= 250) {
            $('.l-regis-section > .js-regis-section__title .js-regis-section__line-art').removeClass('js-regis-section__line-art');
            $(".l-regis-section > .js-regis-section__title").removeClass("js-regis-section__title");
            $(".l-regis-section > .l-regis-section__regis-con > .c-regis-section__slide-button").removeClass("js-regis-section__slide-button");
            titleState = false;
        }
    
        $(".l-regis-section > .l-regis-section__event-con").each(function(index) {
            eventPos = $(this).offset().top - $(window).scrollTop();
            if (regisState[index] && eventPos >= -350 -$(this).height() && eventPos <= 650) {
                $(this).removeClass("js-regis-section__event-con");
                $(this).find(".c-open-regis__line-art").removeClass("js-open-regis__line-art")
                $(this).find(".js-open-regis__label").removeClass("js-open-regis__label")
                $(this).find(".js-regis-section__name").removeClass("js-regis-section__name")
                $(this).find(".l-regis-section__detail").removeClass("js-regis-section__detail")

                $(this).find(".js-countdown-timer__part").removeClass("js-countdown-timer__part")
                regisState[index] = false;
            }
        });
    }
}

function getEvent(type, name, place, date, etc) {
    instance = ``;
    if (type == "workshop") {
        etc[0] = etc[0].split("|");
        instance = `
            <div class="l-regis-section__agency l-row--left">
                <div class="c-regis-section__agency-icon l-regis-section__icon l-image--default">
                </div>
                <p>
                    ${etc[1].replace(/\n/g, "<br/>")}
                </p>
            </div>
        `;
        additional = `
            <div class="l-regis-section__speaker l-row--left">
                <div class="c-regis-section__speaker-icon l-regis-section__icon l-image--default">
                </div>
                <p>
                    ${etc[2].replace(/\n/g, "<br/>")}
                </p>
            </div>
            <div class="l-regis-section__req l-row--left">
                <div class="c-regis-section__req-icon l-regis-section__icon l-image--default">
                </div>
                <p>
                    ${etc[3].replace(/\n/g, "<br/>")}
                </p>
            </div>
            <div class="l-regis-section__regis l-row--left">
                <div class="c-regis-section__regis-icon l-regis-section__icon l-image--default">
                </div>
                <p>
                    ${etc[4].replace(/\n/g, "<br/>")}
                </p>
            </div>
        `;
    } else if (type == "etc") {
        additional = `
        `;
    } else {
        additional = `
            <div class="l-regis-section__speaker l-row--left">
                <div class="c-regis-section__speaker-icon l-regis-section__icon l-image--default">
                </div>
                <p>
                    ${etc[0].replace(/\n/g, "<br/>")}
                </p>
            </div>
        `;
    }

    name = name.split("\n");
    if (name.length == 1) {
        topics = `"${name[0].replace(/\n/g, '<br/>')}"`;
    } else {
        topics = `"${name[0].replace(/\n/g, '<br/>')}" (1<sup>st</sup> Session)`;
    }

    for (nameIndex = 1; nameIndex < name.length; nameIndex++) {
        if (name[nameIndex] != '\n') {
            topics += `<br/><br/>"${name[nameIndex].replace(/\n/g, '<br/>')}" (2<sup>nd</sup> Session)`;
        }
    }

    return `
        <div class="js-regis-section__name js-animation">
            <h3 class="c-regis-section__name">
                ${topics}
            </h3>
        </div> 
        <div class="l-regis-section__detail">
            ${instance}
            <div class="l-regis-section__place l-row--left">
                <div class="c-regis-section__place-icon l-regis-section__icon l-image--default">
                </div>
                <p>
                    ${place.replace(/\n/g, "<br/>")}
                </p>
            </div>
            <div class="l-regis-section__date l-row--left">
                <div class="c-regis-section__date-icon l-regis-section__icon l-image--default">
                </div>
                <p>
                    ${date.replace(/\n/g, "<br/>")}
                </p>
            </div>
            ${additional}
            <a class="c-register-button" href="/registration/${type}/${name[0].toLowerCase().replace(/ /g, "-")}">
                <label class="l-row--center">Register</label>
                <div class="c-border"></div>
                <div class="c-register-button-art"></div>
            </a>
        </div>
    `
}

function setRegisTime(type, name, place, date, etc, openRegisTime, index) {
    currentTime = new Date().getTime();
    distance = openRegisTime - currentTime;
    if (distance <= 0) {
        $(`#${type} .l-regis-section__event[data-id="${index}"]`).html(getEvent(type, name, place, date, etc))
        clearInterval(regisTime[type][index]);
    } else {
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        $(`#${type} .l-regis-section__event[data-id="${index}"] > .c-countdown-timer .c-countdown-timer__day > .c-countdown-timer__circle-art`).text(days);
        $(`#${type} .l-regis-section__event[data-id="${index}"] > .c-countdown-timer .c-countdown-timer__hour > .c-countdown-timer__circle-art`).text(hours); 
        $(`#${type} .l-regis-section__event[data-id="${index}"] > .c-countdown-timer .c-countdown-timer__minute > .c-countdown-timer__circle-art`).text(minutes); 
        $(`#${type} .l-regis-section__event[data-id="${index}"] > .c-countdown-timer .c-countdown-timer__second > .c-countdown-timer__circle-art`).text(seconds);           
    }
}

function showEvent(type, name, place, date, etc, openRegisTime, index) {
    if (openRegisTime - new Date().getTime() <= 0) {
        $(`#${type} .l-regis-section__event[data-id="${index}"]`).html(getEvent(type, name, place, date, etc))
        regisTime[type].push(undefined);
    } else {
        currentTime = new Date().getTime();
        distance = openRegisTime - currentTime;
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        $(`#${type} .l-regis-section__event[data-id="${index}"]`).html(`
            <div class="c-countdown-timer l-col--center">
                <div class="c-countdown-timer__head l-col--center">
                    <div class="js-regis-section__name js-animation">
                        <h3 class="c-regis-section__name">
                            ${name.replace(/\n/g, '<br/>')}
                        </h3>
                    </div>
                    <div class="l-countdown-timer__open-regis l-row--center">
                        <div class="c-open-regis__line-art js-open-regis__line-art js-animation"></div>
                        <label class="js-open-regis__label js-animation">Open Registration</label>
                        <div class="c-open-regis__line-art js-open-regis__line-art js-animation"></div>
                    </div>
                </div>
                <div class="c-countdown-timer__timer l-row--center">
                    <div class="c-countdown-timer__day l-col--center js-countdown-timer__part js-animation">
                        <div class="c-countdown-timer__circle-art l-col--center">
                            ${days}
                        </div>
                        <label>
                            DAYS
                        </label>
                    </div>
                    <div class="c-countdown-timer__hour l-col--center js-countdown-timer__part js-animation">
                        <div class="c-countdown-timer__circle-art l-col--center">
                            ${hours}
                        </div>
                        <label>
                            HOURS
                        </label>
                    </div>
                    <div class="c-countdown-timer__minute l-col--center js-countdown-timer__part js-animation">
                        <div class="c-countdown-timer__circle-art l-col--center">
                            ${minutes}
                        </div>
                        <label>
                            MINUTES
                        </label>
                    </div>
                    <div class="c-countdown-timer__second l-col--center js-countdown-timer__part js-animation">
                        <div class="c-countdown-timer__circle-art l-col--center">
                            ${seconds}
                        </div>
                        <label>
                            SECONDS
                        </label>
                    </div>
                </div>
            </div>
        `);
        regisTime[type].push(setInterval(function() {
            setRegisTime(type, name, place, date, etc, openRegisTime, index);
        }, 1000));
    }
}

$(document).ready(function() { 
    $(window).on("scroll", function() {
        animatetingRegis();
    })

    for (type of ["talkshow", "workshop", "etc"]) {
        $.ajax({
            url: `/registration/${type}-data/`,
            method: "GET",
            dataType: "json",
            success: (data) => {
                if (data.success) {
                    maxIter = 0;
                    if (data.type == "etc") {
                        maxIter = 1;
                    } else {
                        maxIter = data[data.type].length;
                    }
                    for (index = 0; index < maxIter ; index++) {
                        if (data.type == "talkshow") {
                            regisTime[data.type].push(undefined);
                        } else {
                            etc = data[data.type][index].regisTime.split(";");
                            var openRegisTime = undefined;
                            if (etc[0] != "-") {
                                openRegisTime = eval(`new Date(${etc[0]}).getTime()`);
                            } else {
                                openRegisTime = (new Date()).getTime();
                            }
        
                            $(`#${data.type} > .l-regis-section__events`).append(`
                                <div class="l-regis-section__event" data-id="${index}">
                                </div>
                            `);
                            showEvent(
                                data.type, 
                                data[data.type][index].name,
                                data[data.type][index].place,
                                data[data.type][index].date,
                                data[data.type][index].etc,
                                openRegisTime, 
                                index);
                        }
                    }
                    fetchState[data.type] = true;
                    regisState.push(true);
                    animatetingRegis();
                }
            }
        });
    }
});