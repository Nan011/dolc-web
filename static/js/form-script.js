$.fn.serializeObject = function()
{
   var o = {};
   var a = this.serializeArray();
   $.each(a, function() {
       if (o[this.name]) {
           if (!o[this.name].push) {
               o[this.name] = [o[this.name]];
           }
           o[this.name].push(this.value || '');
       } else {
           o[this.name] = this.value || '';
       }
   });
   return o;
};

function popupFlyAway() {
    $(".l-popup-section").addClass("js-popup-section");
    $(".c-wall").addClass("js-wall");
}

function isFormValid() {
    var valid = true
    $("form input[data-type='input']").each(function() {
        if ($(this).attr("required") == "required") {
            if ($(this).attr('type') == 'email') {
                var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if (!regex.test($(this).val())) {
                    valid = false;
                    return false;
                }
            } else if (($(this).attr("type") == "checkbox" && !$(this).prop("checked")) || $(this).val() == "") {
                valid = false;
                return false;
            }
        }
    });
    return valid;
}

titleState = true;
formState = true;

function animatetingForm() {
    titlePos = $(".l-form-section .c-form-section__title .c-form-section__line-art").offset().top - $(window).scrollTop();
    formPos = $(".l-form-section > .l-form-section__form-con").offset().top - $(window).scrollTop();
    if (titleState && titlePos >= -250 && titlePos <= 250) {
        $('.l-form-section > .js-form-section__title .js-form-section__line-art').removeClass('js-form-section__line-art');
        $(".l-form-section > .js-form-section__title").removeClass("js-form-section__title");
        $(".l-form-section > .l-form-section__form-con > .c-form-section__slide-button").removeClass("js-form-section__slide-button");
        titleState = false;
    }

    if (formState && formPos >= -350 && formPos <= 650) {
        $(".l-form-section > .l-form-section__form-con").removeClass("js-form-section__form-con");
        formState = false;
    }
}

$(document).ready(function() {
    $(".c-form-section__message:last").css({
        'display': 'none',
        'background-color': '#9e0000',
        'color': '#f1f1f1'
    });

    $("input[data-format='number']").on("keypress", function(e) {
        var keyCode = e.which;
        if (keyCode == 0 || keyCode == 8 || keyCode == 9 || 
            33 <= keyCode && keyCode <= 40 || 
            48 <= keyCode && keyCode <= 57 || 
            43 == keyCode || 46 == keyCode || 44 == keyCode) {
            return ;
        } else {
            e.preventDefault();
        }
    });

    animatetingForm();

    $(window).on("scroll", function() {
        animatetingForm();
    })

    $("input[type='submit']").on("click", function(e) {
        var $submitMessage = $(".c-form-section__message:last")
        if (isFormValid()) {
            $submitMessage.css({
                'display': 'none'
            });
            $(".l-popup-section__content").html(
                `<p class="c-popup-section__message">Are you sure?</p>
                <div class="l-popup-section__button l-row--center">
                    <button class="c-popup-section__left-button">
                        <label>No</label>
                        <div class="c-border"></div>
                        <div class="c-register-button-art"></div>
                    </button>
                    <button class="c-popup-section__right-button">
                        <label>Yes</label>
                        <div class="c-border"></div>
                        <div class="c-register-button-art"></div>
                    </button>
                </div>
                `);
            $(".l-popup-section").removeClass("js-popup-section");
            $(".c-wall").removeClass("js-wall");
        } else {
            $submitMessage.css({
                'display': 'block'
            });
            $submitMessage.text("Please fill out the form correctly");
        }
    });
    
    $(".l-popup-section").on("click", ".c-popup-section__right-button", function() {
        $(".l-popup-section__content").html(
            `<div class="c-loading-screen l-col--center">
                <div class="c-loading-screen__loading"></div>
                <p class="c-loading-screen__message">WAIT</p>
            </div>
            `);
        // var data = $("form").serializeObject();
        // data = {}
        // data["sheet"] = $("form").attr("data-sheet");
        data = new FormData();
        data.append("attachment", $("input[type='file']").prop('files')[0], "attachment.jpg");
        // data['type'] = "file";
        console.log(data);
        $.ajax({
            url: "https://script.google.com/macros/s/AKfycbw5AaChkamPzc2s0G3jbKbWeKaxxhDELGpRBPozQQ/exec",
            type: 'POST',
            processData: false,
            dataType: false,
            data: data,
            success: function(results) {
                console.log(JSON.stringify(results));
            },
            error: function(results) {
                console.log(JSON.stringify(results));
            }
        });

        // $.ajax({
        //     url: "https://script.google.com/macros/s/AKfycbw5AaChkamPzc2s0G3jbKbWeKaxxhDELGpRBPozQQ/exec",
        //     method: "GET",
        //     dataType: "json",
        //     data: data,
        //     success: () => {
        //         // $("input[data-type='input']").val("");
        //         $(".l-popup-section__content").html(
        //             `<div class="c-popup-section__handshake-icon l-image--default"></div>
        //             <p class="c-popup-section__message">
        //                 Your form has been submitted. See you at the event! 
        //             </p>
        //             <div class="l-popup-section__button l-row--center">
        //                 <button class="c-popup-section__left-button">
        //                     <label>Back</label>
        //                     <div class="c-border"></div>
        //                     <div class="c-register-button-art"></div>
        //                 </button>
        //             </div>
        //             `);
        //     }
        // });
    });

    $(".l-popup-section").on("click", ".c-popup-section__left-button", function() {
        popupFlyAway();
    });

    $(".c-wall").on("click", function() {
        popupFlyAway();
    });
});