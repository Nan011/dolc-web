var eventsState = [];
var eventState = true;
function animatetingElement() {
    eventPos = $(".l-event-section").offset().top - $(window).scrollTop();
    if (eventState && eventPos >= -250 && eventPos <= 250) {
        $('.l-event-section > .js-event-section__title .js-event-section__line-art').removeClass('js-event-section__line-art');
        $(".l-event-section > .js-event-section__title").removeClass("js-event-section__title");
        eventState = false;
    }
    $(".l-event-section__event > div[data-type='event']").each(function(index) {
        eventPos = $(this).offset().top - $(window).scrollTop();
        if (eventsState[index] && eventPos >= -350 && eventPos <= 350) {
            $(this).find(".js-profile__picture").removeClass("js-profile__picture");
            $(this).find(".js-profile__image").removeClass("js-profile__image");
            $(this).find(".js-profile__content").removeClass("js-profile__content");
            $(this).find(".js-profile__circle-art").removeClass("js-profile__circle-art");
            $(this).find(".js-profile__rectangle-art").removeClass("js-profile__rectangle-art")
            eventsState[index] = false;
        }
    });
}
$(document).ready(function() {
    $.ajax({
        method: "GET",
        url: `/events/events-data/`,
        success: function(data) {
            if (data.success) {
                events = "";
                for (index = 0; index < data.events.length; index++)  {
                    counter = undefined;
                    if (index >= 9) {
                        counter = index + 1;
                    } else {
                        counter = `0${index + 1}`
                    }
                    if (index % 2 == 0) {
                        events += `<div class="l-profile--0" data-type="event">
                            <div class="l-profile__picture--0 js-profile__picture js-animation">
                                <div class="l-profile__rectangle-art--0 js-profile__rectangle-art js-animation"></div>
                                <div class="c-profile__image--0 js-profile__image js-animation" style="background-image: url('${data.events[index].photo}')"></div>
                                <h1 class="c-page__counter">${counter}</h1>
                            </div>
                            <div class="l-profile__content--0 js-profile__content js-animation">
                                <div class="l-profile__title">
                                    <div class="l-profile__circle-art--0 js-profile__circle-art js-animation">
                                        <div class="l-circle-art__hor">
                                            <div class="c-circle--large"></div>
                                            <div class="c-circle--large"></div>
                                            <div class="c-circle--large"></div>
                                            <div class="c-circle--large"></div>
                                            <div class="c-circle--large"></div>
                                            <div class="c-circle--large"></div>
                                        </div>
                                        <div class="l-circle-art__hor">
                                            <div class="c-circle--large"></div>
                                            <div class="c-circle--large"></div>
                                            <div class="c-circle--large"></div>
                                            <div class="c-circle--large"></div>
                                            <div class="c-circle--large"></div>
                                            <div class="c-circle--large"></div>
                                        </div>
                                    </div>
                                    <h2 class="c-profile__title">
                                        ${data.events[index].title}
                                    </h2>
                                </div>
                                <p class="c-profile__para">
                                    ${data.events[index].desc.substring(0, 300)}...
                                </p>
                                <div class="c-read-more" data-id="${index}">
                                    <div class="c-read-more__circle-art">
                                        <div class="c-circle--medium js-animation"></div>
                                        <div class="c-circle--medium js-animation"></div>
                                        <div class="c-circle--medium js-animation"></div>
                                    </div>
                                    <div class="c-read-more__label js-animation">
                                        Read more
                                    </div>
                                </div>
                            </div>
                        </div>`;
                    } else {
                        events += `<div class="l-profile--1" data-type="event">
                            <div class="l-profile__picture--1 js-profile__picture js-animation">
                                <div class="l-profile__rectangle-art--1 js-profile__rectangle-art js-animation"></div>
                                <div class="c-profile__image--1 js-profile__image js-animation" style="background-image: url('${data.events[index].photo}')"></div>
                                <h1 class="c-page__counter">${counter}</h1>
                            </div>
                            <div class="l-profile__content--1 js-profile__content js-animation">
                                <div class="l-profile__title">
                                    <div class="l-profile__circle-art--1 js-profile__circle-art js-animation">
                                        <div class="l-circle-art__hor">
                                            <div class="c-circle--large"></div>
                                            <div class="c-circle--large"></div>
                                            <div class="c-circle--large"></div>
                                            <div class="c-circle--large"></div>
                                            <div class="c-circle--large"></div>
                                            <div class="c-circle--large"></div>
                                        </div>
                                        <div class="l-circle-art__hor">
                                            <div class="c-circle--large"></div>
                                            <div class="c-circle--large"></div>
                                            <div class="c-circle--large"></div>
                                            <div class="c-circle--large"></div>
                                            <div class="c-circle--large"></div>
                                            <div class="c-circle--large"></div>
                                        </div>
                                    </div>
                                    <h2 class="c-profile__title">
                                        ${data.events[index].title}
                                    </h2>
                                </div>
                                <p class="c-profile__para">
                                    ${data.events[index].desc.substring(0, 300)}...
                                </p>
                                <div class="c-read-more" data-id="${index}">
                                    <div class="c-read-more__circle-art">
                                        <div class="c-circle--medium js-animation"></div>
                                        <div class="c-circle--medium js-animation"></div>
                                        <div class="c-circle--medium js-animation"></div>
                                    </div>
                                    <div class="c-read-more__label js-animation">
                                        Read more
                                    </div>
                                </div>
                            </div>
                        </div>`;   
                    }
                    eventsState.push(true);
                }
                $(".l-event-section__event").append(events);
                animatetingElement();
            }
        }
    });
    
    $(window).on("scroll", function(index) {
        animatetingElement();
    });

    $(".l-event-section__event").on("click", ".c-read-more", function() {
        $.ajax({
            method: "GET",
            url: `/events/events-data/`,
            success: (data) => {
                if (data.success) {
                    // console.log($(this).attr("data-id"));
                    $(".c-popup-section__title").text(data.events[$(this).attr("data-id")].title.toUpperCase());
                    $(".c-popup-section__para").text(data.events[$(this).attr("data-id")].desc);
                    $(".c-place__content").text(data.events[$(this).attr("data-id")].place);
                    $(".c-date__content").html(data.events[$(this).attr("data-id")].date.replace(/\n/g, "<br/>"));
                    $(".c-popup-section__image").css("backgroundImage", `url("${data.events[$(this).attr("data-id")].photo}")`);
                    // $(".l-page").addClass("js-page");
                    // $(".l-page").removeClass("js-animation-delay--0");
                    $(".l-popup-section").removeClass("js-popup-section");
                    $(".c-wall").removeClass("js-wall");
                } else {
                    console.log("Fail to fetch data!");
                }
            }
        });
    });

    $(".c-wall").on("click", function() {
        $(".l-popup-section").addClass("js-popup-section");
        $(".c-wall").addClass("js-wall");
        setTimeout(function() {
            $(".c-popup-section__title").text("");
            $(".c-popup-section__para").text("");
            $(".c-popup-section__image").css("backgroundImage", "");
        }, 800);
        // $(".l-page").removeClass("js-page");
        // $(".l-page").addClass("js-animation-delay--0");
    });
});