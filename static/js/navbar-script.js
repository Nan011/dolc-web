function aboutButtonEvent() {
    $dolcTag =  $("#c-nav__about-dolc-button");
    $teamTag =  $("#c-nav__about-team-button");
    pointerDelay = 300
    if ($(window).width() <= 700) {
        $dolcTag.css({
            'left': '50%',
            'transform': 'translate(-50%, -50%)',
            'pointer-events': 'none'
        });

        $teamTag.css({
            'right': '50%',
            'transform': 'translate(50%, -50%)',
            'pointer-events': 'none'
        });

        $(".c-nav__button__about").on({
            mouseover: function() {
               $dolcTag.css({
                   'transform': 'translate(calc(-150% - 10px), -50%)',
                   'opacity': '1'
               });

               $teamTag.css({
                    'transform': 'translate(calc(150% + 10px), -50%)',
                    'opacity': '1'
                });
                
                setTimeout(function() {
                    $dolcTag.css({
                        'pointer-events': 'visible',
                    });
            
                    $teamTag.css({
                        'pointer-events': 'visible',
                    });
            
                }, pointerDelay)
            },
            mouseleave: function() {
                $dolcTag.css({
                    'transform': 'translate(-50%, -50%)',
                    'opacity': '0',
                    'pointer-events': 'none',
                });
 
                $teamTag.css({
                    'transform': 'translate(50%, -50%)',
                    'opacity': '0',
                    'pointer-events': 'none',
                }); 
            }
        });
    } else {
        $(".c-nav__button__about").css({
            'padding': '40px 0'
        });
        
        $dolcTag.css({
            'left': '50%',
            'transform': 'translate(-50%, -50%)'
        });

        $teamTag.css({
            'left': '50%',
            'transform': 'translate(-50%, -50%)'
        });

        $(".c-nav__button__about").on({
            mouseover: function() {
               $dolcTag.css({
                   'transform': 'translate(-50%, calc(-50% - 50px)',
                   'opacity': '1'
               });

               $teamTag.css({
                    'transform': 'translate(-50%, calc(-50% + 50px))',
                    'opacity': '1'
                });
            },
            mouseleave: function() {
                $dolcTag.css({
                    'transform': 'translate(-50%, -50%)',
                    'opacity': '0'
                });
 
                $teamTag.css({
                    'transform': 'translate(-50%, -50%)',
                    'opacity': '0'
                }); 
            }
        });
    }
}

$('document').ready(function() {
    aboutButtonEvent();

    $('.c-nav-button').on('click', function() {
        if (eval($(this).attr('data-state'))) {
            $('.l-nav--nav-page .l-nav--bottom').removeClass('js-nav--bottom');
            $('.l-page').addClass('js-page');
            $('.c-nav__button').each(function() {
                $(this).removeClass('js-nav__button');
            });
            $(this).attr('data-state', false);
        } else {
            $('.l-nav--nav-page .l-nav--bottom').addClass('js-nav--bottom');
            $('.l-page').removeClass('js-page');
            $('.c-nav__button').each(function() {
                $(this).addClass('js-nav__button');
            });
            $(this).attr('data-state', true);
        }
    });
});
