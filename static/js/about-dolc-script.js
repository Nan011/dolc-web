var frontState = true;
var profileState = true;
function animatetingElement() {
    frontPos = $(".l-front-section").offset().top - $(window).scrollTop();
    profilePos = $(".l-profile-section").offset().top - $(window).scrollTop();
    if (frontState && frontPos <= -($(window).height() / 2) && -($(window).height()) <= frontPos) {
        $(".l-front-section").removeClass("js-front-section");
        $(".l-front-section .js-front-section__logo").removeClass("js-front-section__logo");
        frontState = false;
    } 
    if (profileState && profilePos >= -250 && profilePos <= 250) {
        $(".l-profile-section > .l-profile-section__profile > .l-profile-section__picture > .c-profile-section__line-art--ver").removeClass("js-profile-section__line-art--ver");
        $(".l-profile-section > .l-profile-section__profile > .l-profile-section__picture > .c-profile-section__line-art--hor").removeClass("js-profile-section__line-art--hor");
        $(".l-profile-section > .l-profile-section__profile > .l-profile-section__picture > .c-profile-section__image").removeClass("js-profile-section__image");
        $(".l-profile-section > .l-profile-section__profile > .l-profile-section__picture > .c-circle-art--0").removeClass("js-circle-art--0");
        $('.l-profile-section > .l-profile-section__profile > .js-profile-section__title .js-profile-section__line-art').removeClass('js-profile-section__line-art');
        $(".l-profile-section > .l-profile-section__profile > .js-profile-section__title").removeClass("js-profile-section__title");
        $(".l-profile-section > .l-profile-section__profile > .c-profile-section__para").removeClass("js-profile-section__para");
        profileState = false;
    }
}
$(document).ready(function() {
    animatetingElement();
    $(window).on("scroll", function() {
        animatetingElement();
    });
});