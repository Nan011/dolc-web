var aboutState = true;
var frontState = true;
var registerState = true;
var testiState = true;
var sponMediaState = true;
var partisState = [];

function animatetingElement() {
    frontPos = $('.l-front-section').offset().top - $(window).scrollTop();
    aboutPos = $('.l-about-section').offset().top - $(window).scrollTop();
    registerPos = $('.l-register-section').offset().top - $(window).scrollTop();
    testiPos = $('.l-testi-section').offset().top - $(window).scrollTop();
    sponMediaPos = $('.l-spon-media-section').offset().top - $(window).scrollTop();

    if (frontState && frontPos >= -400 && frontPos <= 250) {
        $('.l-front-section').removeClass('js-front-section');
        $('.l-front-section .js-front-section__title').removeClass('js-front-section__title');
        $('.l-front-section .js-front-section__line').removeClass('js-front-section__line');
        $('.l-front-section .js-front-section__date').removeClass('js-front-section__date');
        frontState = false;
    }

    if (aboutState && aboutPos >= -250 && aboutPos <= 250) {
        $('.l-about-section .js-profile__picture').removeClass('js-profile__picture');
        $('.l-about-section .js-profile__image').removeClass('js-profile__image');
        $('.l-about-section .js-profile__content').removeClass('js-profile__content');
        $('.l-about-section .js-profile__circle-art').removeClass('js-profile__circle-art');
        $('.l-about-section .js-profile__rectangle-art').removeClass('js-profile__rectangle-art');
        aboutState = false;
    }

    // console.log(registerPos + ', top: ' + -($(window).height() / 2) + ', bot: ' + 0);
    if (registerState && registerPos >= -(3 * $(window).height() / 2) && registerPos <= 0) {
        $('.l-register-section').removeClass('js-register-section');
        $('.l-register-section__left').removeClass('js-register-section__left');
        $('.l-register-section__left .js-register-section__title').removeClass('js-register-section__title');
        $('.l-register-section__left .js-register-section__para').removeClass('js-register-section__para');
        $('.l-register-section__right').removeClass('js-register-section__right');
        registerState = false;
    }

    $(".l-testi-section > .l-testi-section__testi > div[data-type='event']").each(function(index) {
        partiPos = $(this).offset().top - $(window).scrollTop();
        if (partisState[index] && partiPos >= -350 && partiPos <= 350) {
            $(this).find('.js-profile__picture').removeClass('js-profile__picture');
            $(this).find('.js-profile__image').removeClass('js-profile__image');
            $(this).find('.js-profile__content').removeClass('js-profile__content');
            $(this).find('.js-profile__circle-art').removeClass('js-profile__circle-art');
            $(this).find('.js-profile__rectangle-art').removeClass('js-profile__rectangle-art');
            partisState[index] = false;
        }
    });

    if (testiState && testiPos >= -250 && testiPos <= 250) {
        $('.l-testi-section > .js-page__counter').removeClass('js-page__counter');
        $('.l-testi-section > .js-testi-section__title').removeClass('js-testi-section__title');
        $('.l-testi-section > .c-testi-section__title > .js-testi-section__line-art').removeClass('js-testi-section__line-art');
        testiState = false;
    }

    if (sponMediaState && sponMediaPos >= -(3 * $(window).height() / 2) && sponMediaPos <= 0) {
        $('.l-spon-media-section').removeClass('js-spon-media-section');
        $('.l-spon-media-section .js-spon-media-section__sponsor').removeClass('js-spon-media-section__sponsor');
        $('.l-spon-media-section .js-spon-media-section__media').removeClass('js-spon-media-section__media');
        testiState = false;
    }

}
$(document).ready(function() {
    function setImage(img, type, areaTarget, index) {
        img.onload = function() {
            var k = Math.sqrt(areaTarget / (img.width * img.height))
            var $imgTag = $(`.l-spon-media-section__${type}__content > img[data-id="${index}"]`);
            $imgTag.attr("src", img.src);
            $imgTag.css("width", img.width * k);
            $imgTag.css("height", img.height * k);
        }
    }

    for (type of ["sponsor", "media"]) {
        $.ajax({
            url: `/supporter/${type}-data/`,
            method: "GET",
            dataType: "json",
            success: (data) => {
                if (data.success) {
                    var suppData = [];
                    for (index = 0; index < data[data.type].length; index++) {
                        if (data[data.type][index].sizeCategory == "L") {
                            suppData.push(data[data.type][index]);
                        }
                    }

                    for (index = 0; index < data[data.type].length; index++) {
                        if (data[data.type][index].sizeCategory == "M") {
                            suppData.push(data[data.type][index]);
                        }
                    }

                    for (index = 0; index < suppData.length; index++) {
                        $(`.l-spon-media-section__${data.type}__content`).append(`<img data-id=${index} style="margin: 10px;">`);
                        var img = new Image();
                        areaTarget = eval(suppData[index].size.replace(" ", "*"));
                        setImage(img, data.type, areaTarget, index);   
                        img.src = suppData[index].img;
                    }
                }
            }
        });
    }

    $.ajax({
        method: 'GET',
        url: '/participants/participants-data/',
        success: function(data) {
            if (data.success) {
                participants = '';
                len = 2;
                if (data.participants.length < 2) {
                    len = data.participants.length;
                }
                for (index = 0; index < len; index++) {
                    participants += `<div class='l-profile--1' data-type='event'>
                        <div class='l-profile__picture--1 js-profile__picture js-animation'>
                            <div class='l-profile__rectangle-art--1 js-profile__rectangle-art js-animation'>
                            </div>
                            <div class='c-profile__image--1 js-profile__image js-animation' style="background-image: url('${data.participants[index].photo}')">
                            </div>
                        </div>
                        <div class='l-profile__content--1 js-profile__content js-animation'>
                            <div class='l-profile__title'>
                                <div class='l-profile__circle-art--1 js-profile__circle-art js-animation'>
                                    <div class='l-circle-art__hor'>
                                        <div class='c-circle--large'></div>
                                        <div class='c-circle--large'></div>
                                        <div class='c-circle--large'></div>
                                        <div class='c-circle--large'></div>
                                        <div class='c-circle--large'></div>
                                        <div class='c-circle--large'></div>
                                    </div>
                                    <div class='l-circle-art__hor'>
                                        <div class='c-circle--large'></div>
                                        <div class='c-circle--large'></div>
                                        <div class='c-circle--large'></div>
                                        <div class='c-circle--large'></div>
                                        <div class='c-circle--large'></div>
                                        <div class='c-circle--large'></div>
                                    </div>
                                </div>
                                <h2 class='c-profile__title'>
                                    ${data.participants[index].name}
                                </h2>
                            </div>
                            <p class='c-profile__para'>
                                "${data.participants[index].comment}"
                            </p>
                        </div>
                    </div>`;
                    partisState.push(true);
                }
                $('.l-testi-section__testi').append(participants);
                animatetingElement();
            }
        }
    });
    animatetingElement();
    $(window).on('scroll', function() {
        animatetingElement();
    });
});
