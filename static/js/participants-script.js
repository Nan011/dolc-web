$(document).ready(function() {
    var currentParticipant = 0;
    var participantState = true;
    function validateParticipantPosition() {
        if (currentParticipant < 0) {
            currentParticipant = $(".l-participants-section__participants > .l-participants-section__profile").length - 1;
        } else if (currentParticipant >= $(".l-participants-section__participants > .l-participants-section__profile").length) {
            currentParticipant = 0;
        }
    }

    function animatetingParticipants(appearAnimation) {
        current = $(".l-participants-section__profile").eq(currentParticipant);
        participantPos = $(".l-participants-section").offset().top - $(window).scrollTop();
        if (participantState && participantPos >= -350 && participantPos <= 350) {
            $('.l-participants-section > .js-participants-section__title .js-participants-section__line-art').removeClass('js-participants-section__line-art');
            $(".l-participants-section > .js-participants-section__title").removeClass("js-participants-section__title");
            $(".l-participants-section > .l-participants-section__participants-con").removeClass("js-participants-section__participants-con")
            $(".l-participants-section > .l-participants-section__participants-con > .c-participants-section__slide-button").removeClass("js-participants-section__slide-button");
            $(".l-participants-section__profile").each(function() {
                $(this).find(".c-profile__image--1").removeClass("js-profile__image");
            })
            participantState = false;
        }  
        if (appearAnimation) {
            current.find(".l-participants-section__picture").removeClass("js-participants-section__picture");
            current.find(".l-participants-section__content").removeClass("js-participants-section__content");
            current.find(".c-expertise__line-art").removeClass("js-expertise__line-art");   
        } else {
            current.find(".l-participants-section__picture").addClass("js-participants-section__picture");
            current.find(".l-participants-section__content").addClass("js-participants-section__content");     
            current.find(".c-expertise__line-art").addClass("js-expertise__line-art");
        }
    }

    function setSize(img, index) {
        var areaTarget = 0;
        if ($(window).width() <= 700) {
            areaTarget = 150 * 150;
        } else {
            areaTarget = 200 * 200;
        }

        img.onload = function() {
            var k = Math.sqrt(areaTarget / (img.width * img.height));
            var $imgTag = $(`.c-participants-section__image[data-id=${index}]`);
            $imgTag.attr("src", img.src);
            $imgTag.css("width", img.width * k);
            $imgTag.css("height", img.height * k);
        }
    }

    $.ajax({
        method: "GET",
        url: `/participants/agency-data/`,
        success: function(data) {
            if (data.success) {
                participantPriority = {
                    "ASSEGAF HAMZAH & PARTNERS": 0,
                    "HANAFIAH PONGGAWA & PARTNERS": 1,
                    "WIDYAWAN & PARTNERS LAW FIRM": 2,
                    "GINTING & REKSODIPUTRO": 3,
                    "HISWARA BUNJAMIN & TANDJUNG": 4,
                    "LUBIS GANIE SUROWIDJOJO": 5,
                    "ARIYANTO ARNALDO LAW FIRM": 6,
                    "MAKES & PARTNERS LAW FIRM": 7,
                }
                participants = []
                for (index = 0; index < data.agency.length; index++)  {
                    participants[participantPriority[data.agency[index].name]] = data.agency[index];
                }
                for (index = 0; index < participants.length; index++)  {
                    $(".l-participants-section__participants").append(`
                        <div class="l-participants-section__profile l-col--center">
                            <div class="l-participants-section__agency l-col--center">
                                <div class="l-participants-section__picture js-participants-section__picture js-animation">
                                    <img class="c-participants-section__image" data-id="${index}">
                                </div>
                                <div class="l-participants-section__content l-col--center js-participants-section__content js-animation">
                                    <div class="c-participants-section__name">
                                        ${participants[index].name}
                                    </div>
                                    <div class="c-participants-section__expertise l-row--center">
                                        <div class="c-expertise__line-art js-expertise__line-art js-animation"></div>
                                        <label>Expertises</label>
                                        <div class="c-expertise__line-art js-expertise__line-art js-animation"></div>
                                    </div>
                                    <div class="c-participants-section__desc">
                                        ${participants[index].desc.replace(/\n/g, "<br/>")}
                                    </div>
                                </div>                    
                            </div>
                        </div>
                    `);
                    var img = new Image();
                    setSize(img, index);
                    img.src = participants[index].photo;
                }
                animatetingParticipants(true);
            }
        }
    });

    // $(".l-participants-section__circle-art").on("click", ".l-participants-section__circle--large", function() {
    //     animatetingParticipants(false);
    //     $(".l-participants-section__participants").css("transform", `translateX(-${eval($(this).attr("data-id")) * 100 }vw)`);
    //     currentParticipant = eval($(this).attr("data-id"));
    //     setTimeout(function() {
    //         animatetingParticipants(true);
    //     }, 300); 
    // })

    $(".c-participants-section__left-button").on("click", function() {
        animatetingParticipants(false);
        currentParticipant -= 1;
        validateParticipantPosition();
        $(".l-participants-section__participants").css("transform", `translateX(-${currentParticipant * 100 }vw)`);
        setTimeout(function() {
            animatetingParticipants(true);
        }, 300); 
    });

    $(".c-participants-section__right-button").on("click", function() {
        animatetingParticipants(false);
        currentParticipant += 1;
        validateParticipantPosition();
        $(".l-participants-section__participants").css("transform", `translateX(-${currentParticipant * 100 }vw)`);
        setTimeout(function() {
            animatetingParticipants(true);
        }, 300); 
    });
});