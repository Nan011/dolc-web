var frontState = true;
var teamState = true;
function animatetingElement() {
    frontPos = $(".l-front-section").offset().top - $(window).scrollTop();
    teamPos = $(".l-team-section").offset().top - $(window).scrollTop();

    if (frontState && frontPos >= -400 && frontPos <= 400) {
        $(".l-front-section").removeClass("js-front-section");
        $(".l-front-section .js-front-section__logo").removeClass("js-front-section__logo");
        frontState = false;
    } 
    if (teamState && teamPos >= -250 && teamPos <= 250) {
        $(".l-team-section > .l-team-section__team > .c-team-section__image").removeClass("js-team-section__image");
        $(".l-team-section > .l-team-section__team > .c-circle-art--0").removeClass("js-circle-art--0");
        $('.l-team-section > .js-team-section__title .js-team-section__line-art').removeClass('js-team-section__line-art');
        $(".l-team-section > .js-team-section__title").removeClass("js-team-section__title");
        teamState = false;
    }
}
$(document).ready(function() {
    animatetingElement();
    $(window).on("scroll", function() {
        animatetingElement();
    });
});