from django.shortcuts import render
from django.http import JsonResponse
from participants import models
# Create your views here.
def parti(request):
    data = {}
    return render(request, "main-pages/parti.html", data)


def participants(request):
    data = {
        "success": False,
    }
    
    if request.method == "GET":
        participants = models.Participant.objects.all()
        if participants.first() != None:
            data["participants"] = []
            for participant in participants:
                data["participants"].append({
                    "name": participant.name, 
                    "photo": participant.photo.url, 
                    "comment": participant.comment})
            data["success"] = True

    return JsonResponse(data)

def agency(request):
    data = {
        "success": False,
    }
    if request.method == "GET":
        agency = models.Agency.objects.all()
        if agency.first() != None:
            data["agency"] = []
            for participant in agency:
                data["agency"].append({
                    "name": participant.name, 
                    "photo": participant.photo.url, 
                    "desc": participant.desc})
            data["success"] = True

    return JsonResponse(data)