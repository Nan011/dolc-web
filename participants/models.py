from django.db import models

# Create your models here.
class Participant(models.Model):
    name = models.CharField(max_length=50)
    comment = models.TextField(max_length=300)
    photo = models.ImageField(upload_to="imgs/participants/", blank=True, null=True)
    def __str__(self):
        return self.name

class Agency(models.Model):
    name = models.CharField(max_length=50)
    desc = models.TextField(max_length=300)
    photo = models.ImageField(upload_to="imgs/agencies/", blank=True, null=True)
    def __str__(self):
        return self.name