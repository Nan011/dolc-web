from django.contrib import admin
from participants import models
# Register your models here.
admin.site.register(models.Participant)
admin.site.register(models.Agency)
