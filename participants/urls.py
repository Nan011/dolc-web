from django.urls import path
from participants import views

app_name = "parti"
urlpatterns = [
    path("", views.parti, name="parti"),
    path("participants-data/", views.participants),
    path("agency-data/", views.agency),
]