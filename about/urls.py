from django.urls import path
from about import views

app_name = "about"
urlpatterns = [
    path("", views.dolc),
    path("dolc/", views.dolc, name="dolc"),
    path("team/", views.team, name="team"),
]