from django.shortcuts import render

# Create your views here.
def dolc(request):
    data = {}
    return render(request, "main-pages/about-dolc.html")

def team(request):
    data = {}
    return render(request, "main-pages/about-team.html")