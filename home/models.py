from django.db import models

# Create your models here.
class SupporterSize(models.Model):
    name = models.CharField(max_length = 50)
    size = models.CharField(max_length = 50)
    def __str__(self):
        return self.name

class SupporterType(models.Model):
    name = models.CharField(max_length = 50)
    def __str__(self):
        return self.name

class Supporter(models.Model):
    img = models.ImageField(upload_to = "imgs/supporters/", null=True)
    name = models.CharField(max_length = 50)
    type = models.ForeignKey(SupporterType, on_delete=models.SET_NULL, null=True)
    size = models.ForeignKey(SupporterSize, on_delete=models.SET_NULL, null=True)
    def __str__(self):
        return self.name