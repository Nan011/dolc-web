from django.urls import path
from home import views

app_name = "home"
urlpatterns = [
    path("", views.index, name="home"),
    path("home/", views.index, name="home"),
    path("supporter/<str:type>-data/", views.get_supporter),
]