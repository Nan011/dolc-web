from django.contrib import admin
from home import models

# Register your models here.
admin.site.register(models.SupporterType)
admin.site.register(models.SupporterSize)
admin.site.register(models.Supporter)