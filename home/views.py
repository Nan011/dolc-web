from django.shortcuts import render
from django.http import JsonResponse
from home import models
from participants.models import Participant
# Create your views here.
def index(request):
    data = {
        "participants": Participant.objects.all(),
    }
    return render(request, "main-pages/home.html", data)

def get_supporter(request, type):
    data = {
        "success": False,
    }
    if request.method == "GET" and (type.lower() in ["sponsor", "media"]):
        supporters = models.Supporter.objects.filter(type__name__contains=type.capitalize())
        print(supporters)
        if supporters.first() != None:
            data["type"] = type
            data[type] = []
            for supporter in supporters:
                data[type].append({
                    "img": supporter.img.url, 
                    "name": supporter.name, 
                    "type": supporter.type.name,
                    "sizeCategory": supporter.size.name,
                    "size": supporter.size.size
                })
            data["success"] = True
            
    return JsonResponse(data)